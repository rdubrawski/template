# Simple Makefile for building BigInt project code.
# Where are we (needed later)
CWD := $(shell pwd)
# OS Dependent library 
UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
PROC := $(shell uname -p)
ifeq ($(PROC), x86_64)
LIBDIR += -Lgtest/linux-amd64
else
LIBDIR += -Lgtest/linux-x86
endif
else
LIBDIR += -Lgtest/osx
endif
# Tools (if needed)
CXX = g++

ifeq ($(PROFILE), y)
CXXFLAGS += -ftest-coverage -fprofile-arcs
LDFLAGS += -fprofile-arcs
else 
ifeq ($(DEBUG), y)
CXXFLAGS += -g
endif
endif

LIBS += -lgtest -lpthread

SRCDIR := src
INCDIR := inc
OBJDIR := obj
COVDIR := tst
COVINFO := coverage.info

INCPATH += -I$(INCDIR)/ -Igtest/include/
TGT_BIN := tgt_bin
TGT_OBJS := $(OBJDIR)/obj1.o 


.PHONY : all
all: $(TGT_BIN)

.PHONY : clean
clean :
	rm -rf $(OBJDIR) 
	rm -f $(TGT_BIN)
	rm -f $(COVINFO)
	rm -f $(COVINFO).full
	rm -rf $(COVDIR)

$(OBJDIR) :
	@mkdir -p $(OBJDIR)

$(TGT_BIN) : $(TGT_OBJS)
	@echo "Linking $(TGT_OBJS) to buld $(TGT_BIN)"
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBDIR) $(LIBS)

$(COVDIR) :
	@mkdir -p $(COVDIR)

.PHONY : coverage
ifeq ($(PROFILE), y)
coverage : $(TGT_BIN) $(COVDIR)
	@echo "--- Running $(TGT_BIN) and generating coverage data"
	./$(TGT_BIN)
	@lcov -b . -d $(OBJDIR) -c -o $(COVINFO).full
	@lcov -o $(COVINFO) -e $(COVINFO).full "$(CWD)/$(SRCDIR)/*" "$(CWD)/$(INCDIR)/*"
	@genhtml  --highlight --legend --output-directory $(COVDIR) coverage.info
	@echo "-- Use $(COVDIR)/index.html to view results"
else
coverage :
	@echo "Cannot use coverage target without PROFILE=y"
endif
# General rules
obj/%.o : src/%.cpp $(OBJDIR)
	@echo "Compiling $<"
	$(CXX) -c $(CXXFLAGS) $(INCPATH) $< -o $@


